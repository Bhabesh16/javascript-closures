function counterFactory() {
  let counter = 0;

  const increment = () => {
    return ++counter;
  };

  const decrement = () => {
    return --counter;
  };

  const counterObject = {
    increment: increment,
    decrement: decrement,
  };

  return counterObject;
}

module.exports = counterFactory;
