function limitFunctionCallCount(cb, n) {
  if (typeof cb !== "function" || typeof n !== "number") {
    throw new Error("Parameter is not passed correctly!");
  }

  function invokeFunctionLimit(...args) {
    if (n > 0) {
      n--;
      return cb(...args);
    } else {
      return null;
    }
  }

  return invokeFunctionLimit;
}

module.exports = limitFunctionCallCount;
