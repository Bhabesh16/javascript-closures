const limitFunctionCallCount = require("../limitFunctionCallCount.cjs");

const number = 5;

function cb(...args) {
  return "callback function invoked.";
}

try {
  const invokeFunctionLimit = limitFunctionCallCount(cb, number);

  console.log(invokeFunctionLimit(1, 2, 3, 4, 5, 6));
  console.log(invokeFunctionLimit());
  console.log(invokeFunctionLimit());
  console.log(invokeFunctionLimit());
  console.log(invokeFunctionLimit());
  console.log(invokeFunctionLimit());
  console.log(invokeFunctionLimit());
} catch (e) {
  console.error(e);
}
