const cacheFunction = require("../cacheFunction.cjs");

function cb(...args) {
  return parseInt(Math.random() * 10000);
}

try {
  const invokeCacheFunction = cacheFunction(cb);

  console.log(invokeCacheFunction(10, 40, 50));
  console.log(invokeCacheFunction(10, 40, 50));
  console.log(invokeCacheFunction(4));
  console.log(invokeCacheFunction(4));
} catch (e) {
  console.error(e);
}
